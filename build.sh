#Script to build zImage
#By Rachit Rawat @XDA

#set GCC variable
GCC="/home/rachit/android/toolchains/linaro/bin/arm-linux-androideabi"

#Build zimage
ARCH=arm CROSS_COMPILE=$GCC- make sa77_defconfig

#Hash the next line to skip compiling and generate just the .config
ARCH=arm CROSS_COMPILE=$GCC- make -j8

