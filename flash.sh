#!/bin/bash
cd ../
cd out

ADB_WAIT="adb wait-for-device"

while :
do
echo " Hi! What you want to do "
echo " 1. Fastboot Flash Single Recovery Kernel "
echo " 2. Push Kernel zip to /sdcard "
echo " 3. Push External_Modules.zip to /sdcard "
echo " 4. adb sideload zip "
echo " 5. Exit! "

read n
case $n in
        1) 
            echo " Flashing Single recovery kernel"
            unzip -qqo Kernel-Vengeance-Taoshan
            fastboot flash boot boot.img
            ;;
        2)  
            eval $ADB_WAIT
            echo " Pushing Single recovery kernel"
            adb push Kernel-Vengeance-Taoshan.zip /sdcard/
            ;;
        3)
            eval $ADB_WAIT
            echo " Pushing modules to sdcard "
            adb push External_Modules.zip /sdcard/
            echo " Done! "
            ;;
        4) 
              echo "sideloading zip..."
              adb sideload Kernel-Vengeance-Taoshan.zip
              ;;
        5)
          exit
          ;;
        *) echo " Invalid option";;
    esac
done
