#Kernel Pack Script
#By Rachit Rawat- XDA

#Set up variables
MOD_PATH="system/lib/modules"
MODULES="/home/rachit/android/taoshan/jb/meta/system/lib/modules"
DIR="/home/rachit/android/taoshan/jb"
GCC="/home/rachit/android/toolchains/linaro/bin/arm-linux-androideabi"

#Check for working directory
if [ -d ../out ]
then
echo " Working directory found! "
echo " Cleaning working directory "
rm -rf ../out/*
else 
echo " Working directory not found! Creating it "
mkdir ../out
fi

#Check for ext-modules directory
if [ -d ../ext_modules ]
then
rm -rf ../ext_modules/$MOD_PATH/*
fi

#Get all the modules
echo " Getting compiled modules "
rm -rf $MODULES/*
find . -name '*ko' -exec cp '{}' $MODULES \;

#Strip Modules
cd $MODULES
echo " Stripping modules "
$GCC-strip --strip-unneeded *.ko
mkdir prima
mv wlan.ko prima/prima_wlan.ko

#ext-modules
mv hid-sony.ko $DIR/ext_modules/$MOD_PATH/hid-sony.ko
mv cifs.ko $DIR/ext_modules/$MOD_PATH/cifs.ko
mv ntfs.ko $DIR/ext_modules/$MOD_PATH/ntfs.ko
mv nls_utf8.ko $DIR/ext_modules/$MOD_PATH/nls_utf8.ko
cd $DIR/ext_modules
zip -9rq External_Modules.zip *
mv External_Modules.zip ../out/
cd $DIR

#Recovery ramdisk
cd recovery
echo " Creating recovery ramdisk "
find ./ | cpio --quiet -H newc -o > ../recovery.cpio
xz -qe ../recovery.cpio
#lzop -9qUf ../recovery.cpio
#gzip -9qf ../recovery.cpio
cd ../
cp recovery.cpio.xz ramdisk/sbin/recovery.cpio.xz
rm recovery.cpio.xz

#Make Single recovery RAMDISK
cd ramdisk
echo " Creating init ramdisk "
#find . | cpio --quiet -o -H newc | lzma -qe > ../ramdisk.img
find . | cpio --quiet -o -H newc | gzip -9qf > ../ramdisk.img
cd ../

#Make boot img
echo " Making Single Recovery kernel "
mkbootimg --base 0x80200000 --kernel kernel/arch/arm/boot/zImage --ramdisk_offset 0x02000000 --cmdline "console=ttyHSL0,115200,n8 androidboot.hardware=qcom user_debug=31 msm_rtb.filter=0x3F ehci-hcd.park=3" --ramdisk ramdisk.img --pagesize 4096 -o boot.img

#Zip boot.img
mv boot.img meta/
cd meta
zip -9rq Kernel-Vengeance-Taoshan.zip *
mv Kernel-Vengeance-Taoshan.zip ../out/
cd ../
rm ramdisk.img

echo " Done! Files placed in out directory."

